
#

__lzXen__ CLI tool for compressing end XOR encoding file.

### WARNING
```
This tool is written by me for learning C lang. Try don't laugh at the code.
```


```console
$ ./lzxen -h

LZMA2 XOR Encoder. Version: v1.3.7

Encrypt/Decrypt archive:
Usage: ./lzxen
    -f <archive_name>
    -s <keydata_name> [ Mode #1; Read keydata from file ]
    -k <pbkdf2> /OR/ -k <argon2> [ Mode #2 ]
      [ Input password and create keydata via PBKDF2 SHA256 /OR/ Argon2i 256 bit ]
    -l {0..9} [ compression level; optional | default value = 9 ]
    -i {1..n} [ nunmber of iterations; optional | default value = 1 ]
    -r {1..n} [ nunmber of compressed ratio; optional | default value = 5 ]
    -e [ for compress archive ]
    /OR/
    -d [ for decompress archive ]

```


```console
$ ./test_run_keyfile.sh
Check encode & decode file/archive.

Let's Encode...
+ ./lzxen -i 10 -l 5 -s k48 -f Makefile -e
Read 'k48' total bytes: 48

Selected keyfile k48.
Selected datafile: Makefile

Selected compression level: 5
Creating lzma archive Makefile ...

Start Compress...
Read 'Makefile' total bytes: 1265

Buffer size: 1265 bytes

Compressing Data -> Done!

Encoded data size: 452 bytes
Writing file 'Makefile.lzma' -> Done!

Let's Decode...
+ ./lzxen -i 10 -s k48 -f Makefile.lzma -d
Read 'k48' total bytes: 48

Selected keyfile k48.
Selected datafile: Makefile.lzma
Decompressing lzma archive Makefile.lzma ...

Start Decompress...
Read 'Makefile.lzma' total bytes: 452

Buffer size: 452 bytes

Decompressing...
Decompressed size bytes: 1265

Decompressed
Decoded data size: 1265 bytes
Writing file 'Makefile.lzma.bin' -> Done!

Verification of two files for identity...
Files Makefile and Makefile.lzma.bin are identical

Let's Clean...
Remove Makefile.lzma is done.
Remove Makefile.lzma.bin is done.
```

```console
$ ./test_run_argon2i.sh
Check encode & decode file/archive.

Let's Encode...
+ ./lzxen -i 10 -l 5 -k argon2 -f Makefile -e

Selected Argon2i generarion keydata.
Please input Password:

 Call Argon2 func ...
Selected datafile: Makefile

Selected compression level: 5
Creating lzma archive Makefile ...

Start Compress...
Read 'Makefile' total bytes: 1265

Buffer size: 1265 bytes

Compressing Data -> Done!

Encoded data size: 452 bytes
Writing file 'Makefile.lzma' -> Done!

Let's Decode...
+ ./lzxen -i 10 -k argon2 -f Makefile.lzma -d

Selected Argon2i generarion keydata.
Please input Password:

 Call Argon2 func ...
Selected datafile: Makefile.lzma
Decompressing lzma archive Makefile.lzma ...

Start Decompress...
Read 'Makefile.lzma' total bytes: 452

Buffer size: 452 bytes

Decompressing...
Decompressed size bytes: 1265

Decompressed
Decoded data size: 1265 bytes
Writing file 'Makefile.lzma.bin' -> Done!

Verification of two files for identity...
Files Makefile and Makefile.lzma.bin are identical

Let's Clean...
Remove Makefile.lzma is done.
Remove Makefile.lzma.bin is done.
```

### Compiling on Windows using MSYS2/Cygwin

Get repo:
```console
git clone https://gitlab.com/r3dbU7z/lzxen.git
```

Build fast-lzma2 lib:

```console
git clone https://github.com/conor42/fast-lzma2.git
cd ./fast-lzma2/
make
cp libfast-lzma2.dll libfast-lzma2.a ../lzxen
```

Build argon2 lib:

```console
git clone https://github.com/P-H-C/phc-winner-argon2.git
cd ./phc-winner-argon2/
make
cp argon2.h libargon2.dll libargon2.a ../lzxen
```

Get PKCS5_PBKDF2 source file:

```console
git clone https://github.com/kholia/PKCS5_PBKDF2.git
cd ./PKCS5_PBKDF2/
cp pbkdf2-sha256.c ../lzxen
```

Build lzXen:
```console
cd ./lzxen
make
```

Build a statically linked version:
```console
make static
```

### Requirements:

* 'fast-lzma2' library by @conor42
The Fast LZMA2 Library is a lossless high-ratio data compression library based on Igor Pavlov's LZMA2 codec from 7-zip.

[GitHub][https://github.com/conor42/fast-lzma2]

Fast LZMA2 is dual-licensed under BSD and GPLv2.

* 'pbkdf2-sha256' library by @DhiruKholia
Standalone PKCS5_PBKDF2 implementations. Salt lengths > 16 is handled correctly.

Repository without LICENSE file.

[GitHub][https://github.com/kholia/PKCS5_PBKDF2]

* 'libargon2' provides an API to both low-level and high-level functions for using Argon2.

[GitHub][https://github.com/P-H-C/phc-winner-argon2]

> Intellectual property
Except for the components listed below, the Argon2 code in this repository is copyright (c) 2015 Daniel Dinu, Dmitry Khovratovich (main authors), Jean-Philippe Aumasson and Samuel Neves, and dual licensed under the CC0 License and the Apache 2.0 License. For more info see the LICENSE file.
The string encoding routines in src/encoding.c are copyright (c) 2015 Thomas Pornin, and under CC0 License.
The BLAKE2 code in src/blake2/ is copyright (c) Samuel Neves, 2013-2015, and under CC0 License.
All licenses are therefore GPL-compatible.

See also README.md

### Inspired 'UPX Fixer' by @lcashdol / Larry W. Cashdollar

[GitHub][https://github.com/lcashdol/UPX]

### License

[GPLv3](COPYING)