/*
bytes_coupler.h
Nibbles coupler / mixer
*/
#include <stdio.h>
#include <stdlib.h>

// 'Struct' defined in flzma2.c
/*struct filestore {
    unsigned char *data;
    int f_size;
};
typedef struct filestore Struct;*/

// Clutch the byte chain
/*Struct nibblesCoupler(Struct compressData) {
    // Make union for bits operation
    union code
    {
        uint8_t bitsPart; // representation as 'Byte'
        struct { // representation as 'Nibbles'
            // 4 bit parts
            unsigned int x:4;
            unsigned int y:4;
        }; 
    };
    typedef union code Union;
    Union c;
    Union a;
    Union b; // temp store
    for (int i = 1; compressData.f_size > i; ++i) {
        c.bitsPart = compressData.data[i]; // 'Second' byte
        a.bitsPart = compressData.data[i - 1]; // 'First' byte
        // Swap nibbles
        b.x = a.x; // store Litte-part(4-bit) from 'First' byte
        a.x = ~c.y; // reversing source bits and exchange; Big-part from 'Second' byte -> to Little-part to 'First' byte 
        c.y = ~b.x; // reversing source bits and exchange; Litte-part from 'First' byte -> to Big-part to 'Second' byte 
        compressData.data[i] = c.bitsPart;
        compressData.data[i - 1] = a.bitsPart;
        ++i; // two byte step 
    }
return (compressData);
}*/

// ROR-Right rotate bits
unsigned char 
rotr(unsigned char in_byte, int bit_shift)
    {   
        return ((unsigned char)(in_byte) >> (bit_shift) | (unsigned char)(in_byte) << (8 - (bit_shift)));
    }
// ROL-Left rotate bits
unsigned char 
rotl(unsigned char in_byte, int bit_shift)
    {   
        return ((unsigned char)(in_byte) << (bit_shift) | (unsigned char)(in_byte) >> (8 - (bit_shift)));
    }

Struct 
catenate_bytes(Struct compressData)
    {
        for (int i = 1; compressData.f_size > i; ++i) {
        uint8_t second_byte = compressData.data[i];
        uint8_t first_byte = compressData.data[i - 1];
        uint8_t buffer_1 = ~first_byte >> (4);
        uint8_t buffer_2 = ~second_byte << (4);
        compressData.data[i - 1] = buffer_1 << (4) | second_byte >> (4);
        compressData.data[i] = first_byte << (4) | buffer_2 >> (4);
        ++i; 
        }
        return (compressData);
    }
// Change bits parts in byte 
int 
bit_mixer(int inByte)
    {
        int outByte = 0xff;
        uint8_t buffer_1 = inByte >> (4);
        uint8_t buffer_2 = inByte << (4);
        outByte = buffer_2 | buffer_1;
        return (outByte);
    }