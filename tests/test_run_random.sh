#!/usr/bin/env bash
#
# Script for testing algo/tool
#

# Debug section
set -o errexit
#set -o nounset
set -o pipefail
#set -o xtrace
#set -o verbose

# BANNER
echo "Check encode & decode random data file/archive."

if [ $# -lt 2 -o "$1" == "-f" ]; then
	FILENAME="$2"
fi

if [ $# -lt  1 ]; then
	FILENAME="rnd_data.bin" # default file for test
fi

# Options -i 10 [ Iterations ] -l 5 [ Compression ] -k k48 [ Name Keyfile ]

_encode(){
	# Encode
	printf  "\n"
	printf  "\e[32mLet's Encode...\e[0m\n"
	set -x
	../lzxen -i 10 -l 5 -s k256 -f ${FILENAME} -e
	{ set +x; } &> /dev/null
}

_decode(){
	# Decode
	printf  "\n"
	printf  "\e[32mLet's Decode...\e[0m\n"
	set -x
	../lzxen -i 10 -s k256 -f ${FILENAME}.lzma -d
	{ set +x; } &> /dev/null
}

_verif(){
	# Verif
	printf  "\n"
	printf  "\e[32mVerification of two files for identity...\e[0m\n"
	diff -s ${FILENAME} ${FILENAME}.lzma.bin
}

_clean(){
	# Remove test files
	printf  "\n"
	printf  "\e[32mLet's Clean...\e[0m\n"
	rm ./${FILENAME}.lzma
	printf  "Remove ${FILENAME}.lzma is \e[32mdone.\e[0m\n"
	rm ./${FILENAME}.lzma.bin
	printf  "Remove ${FILENAME}.lzma.bin is \e[32mdone.\e[0m\n"
}

_encode
_decode
_verif
_clean

exit 0