MAKE = make
CC = gcc
STRIP = strip
CFLAGS = -Wall -ggdb -O3 -D DEBUG=0 
LFLAGS = -L./
AR = ar

all	: 
	make	pbkdf2-sha256 f_read flzma2 lzxen pbkdf2-sha256

static	:
	make	pbkdf2-sha256_s f_read_s flzma2_s lzxen_static

lzxen_static	: lzxen.o
	$(CC) -static -O3 lzxen.o $(LFLAGS)  -l:pbkdf2-sha256.a -l:flzma2.a -l:f_read.a -l:libfast-lzma2.a -l:libargon2.a -o lzxen

lzxen	: lzxen.o
	$(CC) -O3 lzxen.o -o lzxen $(LFLAGS) -lf_read -lflzma2 -lpbkdf2-sha256 -llibargon2
	
f_read	: f_read.o
	$(CC) -c -O3  f_read.c -o f_read.o
	$(CC) -shared f_read.o -o f_read.dll

flzma2	: flzma2.o
	$(CC) -c -O3 flzma2.c -o flzma2.o
	$(CC) -shared f_read.o flzma2.o -o flzma2.dll $(LFLAGS) -llibfast-lzma2

pbkdf2-sha256 	: pbkdf2-sha256.o
	$(CC) -c -O3 pbkdf2-sha256.c -o pbkdf2-sha256.o
	$(CC) -shared pbkdf2-sha256.o -o pbkdf2-sha256.dll

f_read_s	: f_read.o
	$(CC) -c -O3  f_read.c -o f_read.o
	$(AR) rcs f_read.a f_read.o

flzma2_s	: flzma2.o
	$(CC) -c -O3 flzma2.c -o flzma2.o
	$(AR) rcs flzma2.a flzma2.o
	
pbkdf2-sha256_s	: pbkdf2-sha256.o
	$(CC) -c -O3 pbkdf2-sha256.c -o pbkdf2-sha256.o
	$(AR) rsc pbkdf2-sha256.a pbkdf2-sha256.o

clean	: 
	@echo "Cleaning up src files."
	@rm -f *.o flzma2.a f_read.a f_read.dll flzma2.dll pbkdf2-sha256.dll lzxen f_read flzma2

strip	:
	$(STRIP) lzxen.exe