/*
keystream_gen.h
Key data generation
*/

// Import from pbkdf2-sha256 lib
extern int PKCS5_PBKDF2_HMAC();

// EDIT THIS ===--->
// Set minimal password lenght
#define MIN_PASSWD_LENGHT 8
#define SALT "+_-=s4LTN0MoR3)=-_+" //  <===---
#define PASS_MAX 128

// Create keydata from password via PBKDF2
Struct get_PBKDF2_keystream ( Struct keystream ) {
        /*    void PKCS5_PBKDF2_HMAC(unsigned char *password, size_t plen,
        unsigned char *salt, size_t slen,
        const unsigned long iteration_count, const unsigned long key_length,
        unsigned char *output)*/

        char *password;
        password = NULL;
        size_t plen = 0;
        char *salt = SALT;
        size_t slen = 0;
        const unsigned long iteration_count = 65536;
        const unsigned long key_length = 256;
        // Get Salt lenght
        slen = strlen(salt);
        // Get password string from the terminal
        password = getpass("Please input Password: ");
        // Get Password lenght
        plen = strlen(password);
        // 
        if ( plen == 0 ) {
            printf("\n\033[0;31mPassword not found.\033[0m \n");
            keystream.data = NULL;
            return(keystream);
        }
        if ( plen <= MIN_PASSWD_LENGHT ) {
            printf("\nPassword is too SHORT. Need more then %d symbols. \n", MIN_PASSWD_LENGHT);
            keystream.data = NULL;
            return(keystream);
        }
        //
        if ( password != NULL || keystream.data != NULL ) {
            // Allocation Memory for keydata
            keystream.data  = (unsigned char *) malloc(sizeof(unsigned long) * key_length);
            // 
            printf("\nCall PKCS5_PBKDF2_HMAC func ...\n");
            PKCS5_PBKDF2_HMAC (password, plen, salt, slen, iteration_count, key_length, keystream.data);
            // Set keysize in struct
            keystream.f_size = (int) key_length;
            for (int x = 0; x < plen; x++) {
                password[x] = 0xff;
            }
        }
        return(keystream);
    }
//
Struct get_argon2_keystream ( Struct keystream ) {

    char *password;
    password = NULL;
    int HASHLEN = 256;
    uint32_t pwdlen = 0;
    uint8_t hash1[HASHLEN];
    uint8_t hash2[HASHLEN];
    uint32_t t_cost = 2;            // 2-pass computation
    uint32_t m_cost = (1<<16);      // 64 mebibytes memory usage
    uint32_t parallelism = 4;       // number of threads and lanes

    // Set Salt and Salt string length
    uint8_t *salt = (uint8_t *) strdup(SALT);
    uint32_t slen = strlen((char *)SALT);

    // Get password string from the terminal
    password = getpass("Please input Password: ");
    // Set Password and Password string length
    uint8_t *pwd = (uint8_t *) password;
    pwdlen = (uint32_t) strlen(password);
    //
    if ( pwdlen == 0 ) {
        printf("\n\033[0;31mPassword not found, abort \033[0m \n");
        keystream.data = NULL;
        return(keystream);
    }
    if ( pwdlen <= MIN_PASSWD_LENGHT ) {
        printf("\nPassword is too SHORT. Need more then %d symbols. \n", MIN_PASSWD_LENGHT);
        keystream.data = NULL;
        return(keystream);
    }
    //
    if ( password != NULL || keystream.data != NULL) {
    // Allocation Memory for keydata
    keystream.data = malloc (( HASHLEN + 1)  *sizeof (char));
    keystream.f_size = HASHLEN;

    printf ("\n Call Argon2 func ...\n");
    // high-level API
    argon2i_hash_raw (t_cost, m_cost, parallelism, pwd, pwdlen, salt, slen, hash1, HASHLEN);

    //
    for (int x = 0; x < keystream.f_size; x++) keystream.data[x] = 0;
    for (int x = 0; x < keystream.f_size; x++) keystream.data[x] = hash1[x];

    // low-level API
    argon2_context context = {
        hash2,  /* output array, at least HASHLEN in size */
        HASHLEN, /* digest length */
        pwd, /* password array */
        pwdlen, /* password length */
        salt,  /* salt array */
        slen, /* salt length */
        NULL, 0, /* optional secret data */
        NULL, 0, /* optional associated data */
        t_cost, m_cost, parallelism, parallelism,
        ARGON2_VERSION_13, /* algorithm version */
        NULL, NULL, /* custom memory allocation / deallocation functions */
        /* by default only internal memory is cleared (pwd is not wiped) */
        ARGON2_DEFAULT_FLAGS
    };

    int rc = argon2i_ctx( &context );
    if(ARGON2_OK != rc) 
    {
        printf("Error: %s\n", argon2_error_message(rc));
        exit(1);
    }
    for (int x = 0; x < pwdlen; x++)
        {
            password[x] = 0xff;
        }
    }
    return(keystream);
}