/*
flzma2.c
Compress & Decompress file with fast-lzma2.dll
Encode & Decode Data

Copyright (C) 2024, 2025 / Author @r3dbU7z
*/

extern int errno;

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>

// Import from 'fast-lzma2' library
extern int FL2_compressMt(); 
extern int FL2_decompressMt(); 
// Set nbThreads for LZMA
const int LZMA2_THREADS_COUNT = 4; // for 4 threads/CPU cores
const char *EXTENSION_ENCODED_FILE = "lzma"; // 
const char *EXTENSION_DECODED_FILE = "bin"; //
//
struct filestore {
    unsigned char *data; // file data
    int f_size; // file size
};
typedef struct filestore Struct;

// Add nibblesCoupler() func
#include "bytes_coupler.h"

// Import from 'f_read' library
extern Struct readfile();

// Compress & Encode
int 
flzma2_compress(char *datafile, Struct keystream, int compressLevel, int enc_iterat_count) {

    // Compressing data 
    printf("\nStart Compress...\n" );
    int x = 0;
    char filename[256];

    // New struct for data file
    Struct srcFile;
    srcFile.data = NULL;
    srcFile.f_size = 0;
    srcFile = (readfile(datafile));

    // Make IV via rand()
    srand(time(NULL));
    //
    char IV[(int) keystream.f_size];
    //printf("Keystream size: %d\n",  keystream.f_size);
    // Fill IV bit
    for (int i = 0; i < (int) keystream.f_size; i++)
    {
        IV[i] = (char) (rand() % 100);
        IV[i] = rotl(IV[i % (int) keystream.f_size], ((i % 8) + 1));
        IV[i] = ~bit_mixer(IV[i]);
    }
    // New struct for IV data
    Struct IV_struct;
    IV_struct.data = NULL;
    IV_struct.f_size = keystream.f_size;
    //
    Struct buff_struct;
    buff_struct.data = NULL;
    buff_struct.f_size = srcFile.f_size + IV_struct.f_size;
    // Resize buffer for files with high entropy value
    buff_struct.f_size = buff_struct.f_size + 256;
    //
    buff_struct.data = malloc ((buff_struct.f_size + 1)  *sizeof (char));
    //
    for (x = 0; x <= buff_struct.f_size; x++) buff_struct.data[x] = 0;
    for (x = 0; x < IV_struct.f_size; x++) buff_struct.data[x] = IV[x];
    for (x = 0; x <= buff_struct.f_size; x++) buff_struct.data[(x + IV_struct.f_size)] = srcFile.data[x];
    //
    for (x = 0; x <= srcFile.f_size; x++) srcFile.data[x] = 0;
    free (srcFile.data);
    // New struct for compressed data
    Struct compressData;
    compressData.data = NULL;
    compressData.f_size = 0;
    compressData.data = malloc ((buff_struct.f_size + 1)  *sizeof (char));
    // Call "FL2_compressMt()" from 'fast-lzma2' lib
    printf("\nBuffer size: %d bytes\n", buff_struct.f_size);
    printf ("\nCompressing Data ->");

    /*  fast-lzma2.h
    FL2LIB_API size_t FL2LIB_CALL FL2_compress(void* dst, size_t dstCapacity,
        const void* src, size_t srcSize, int compressionLevel);    */          // ↓ Restore original file size
        size_t compressed_size = 
        FL2_compressMt(compressData.data, buff_struct.f_size, buff_struct.data, buff_struct.f_size - 256,\
         compressLevel, LZMA2_THREADS_COUNT);
    printf ("\033[0;32m Done!\033[0m\n");
    //
    for (x = 0; x <= buff_struct.f_size; x++) buff_struct.data[x] = 0;
    free (buff_struct.data);
    //
    // Set data size after compress
    compressData.f_size = (int) compressed_size;
    //printf("Compressed size: %ld\n", compressed_size );

    // Make IV via rand()
    srand(time(NULL));
    // Fill IV bits
    for (int i = 0; i < (int) keystream.f_size; i++)
    {
        IV[i] = (char) (rand() % 100);
        IV[i] = bit_mixer(IV[i]);
        IV[i] = ~rotr(IV[i % (int) keystream.f_size], ((i % 8) + 1));   
    }
    //
    buff_struct.f_size = compressData.f_size + IV_struct.f_size;
    //
    buff_struct.data = malloc ((buff_struct.f_size + 1)  *sizeof (char));
    //
    for (x = 0; x <= buff_struct.f_size; x++) buff_struct.data[x] = 0;
    for (x = 0; x < IV_struct.f_size; x++) buff_struct.data[x] = IV[x];
    for (x = 0; x <= buff_struct.f_size; x++) buff_struct.data[(x + IV_struct.f_size)] = compressData.data[x];
    //
    for (x = 0; x <= compressData.f_size; x++) compressData.data[x] = 0;
    free (compressData.data);

    // Main loop
    for (int i = 0; i < enc_iterat_count; ++i) {
        //  Get keystream to struct 'eKey'
        Struct eKey;
        eKey.data = NULL;
        eKey.f_size = keystream.f_size;
        eKey.data = malloc ((eKey.f_size + 1)  *sizeof (char));
        for (x = 0; x <= eKey.f_size; x++) eKey.data[x] = 0;
        for (x = 0; x <= eKey.f_size; x++) eKey.data[x] = keystream.data[x];

        // Clutch the bytes chain
        //compressData = nibblesCoupler(compressData);
        buff_struct = catenate_bytes(buff_struct);
        // encrypt section
        for (x = 0; x <= buff_struct.f_size; x++) {
            // Encode data with XOR algo by key
            buff_struct.data[x] = ~buff_struct.data[x];
            // Make rotr() for byte 'Before' XOR operation
            buff_struct.data[x] = rotr(buff_struct.data[x], ((x % 8) + 1));
            buff_struct.data[x] ^= eKey.data[x % eKey.f_size];
            eKey.data[x % eKey.f_size] = rotl(eKey.data[x % eKey.f_size], ((x % 8) + 1));
            eKey.data[x % eKey.f_size] ^= ~buff_struct.data[x];
            eKey.data[x % eKey.f_size] = bit_mixer(eKey.data[x % eKey.f_size]);
            }
        //
        for (x = 0; x < eKey.f_size; x++) {
            eKey.data[x] = 0xff;
        }
        buff_struct = catenate_bytes(buff_struct);
        free (eKey.data);
    }
    // Wipe keydata
    for (x = 0; x < (int) keystream.f_size; x++) {
        keystream.data[x] = 0xff;
    }
    free(keystream.data);
    
    // Write encoded data to file
    snprintf (filename, 249, "%s.%s", datafile, EXTENSION_ENCODED_FILE);
    FILE* const fout = fopen (filename, "wb");
    if (!fout)  {
        fprintf (stderr, "\033[0;31m Error \033[0m %s %s\n", filename, strerror (errno));
        fclose (fout);
        for (x = 0; x <= buff_struct.f_size; x++) buff_struct.data[x] = 0;
        free (buff_struct.data);
        exit (0);
    }
    printf ("\nEncoded data size: %d bytes\nWriting file '%s' ->", buff_struct.f_size, filename);
    fwrite (buff_struct.data, sizeof buff_struct.data[0], buff_struct.f_size, fout);
     printf ("\033[0;32m Done!\033[0m\n");
    fclose (fout);
    for (x = 0; x <= buff_struct.f_size; x++) buff_struct.data[x] = 0;
    free (buff_struct.data);
    return 0;
}
// Decompress & Decode
int 
flzma2_decompress(char *datafile, Struct keystream, int enc_iterat_count, int compressRatio) {
	//
	const int D_SIZE_MUL = compressRatio; // decompressed data size multiplicator

    // Decompressing data
    printf("\nStart Decompress...\n" );
    char filename[256];
    int x = 0;
    char unsigned *tData = NULL;
    // New struct for compressed data file
    Struct compressData;
    compressData.data = NULL;
    compressData.f_size = 0;
    compressData = (readfile(datafile));

    // Main loop
    for (int i = 0; i < enc_iterat_count; ++i) {
           //  Get keystream to struct 'eKey'
        Struct eKey;
        eKey.data = NULL;
        eKey.f_size = keystream.f_size;
        eKey.data = malloc ((eKey.f_size + 1)  *sizeof (char));
        for (x = 0; x <= eKey.f_size; x++) eKey.data[x] = 0;
        for (x = 0; x <= eKey.f_size; x++) eKey.data[x] = keystream.data[x];
        // Clutch the bytes chain
        //compressData = nibblesCoupler(compressData);
        compressData = catenate_bytes(compressData);
        tData = malloc (sizeof compressData.data[0] *sizeof (char));
        // decrypt section    
        for (x = 0; x <= compressData.f_size; x++) {
            tData[0] = compressData.data[x];
            compressData.data[x] = ~compressData.data[x];
            compressData.data[x] ^= eKey.data[x % eKey.f_size];
            // Make rotl() for byte 'After' XOR operation
            compressData.data[x] = rotl(compressData.data[x], ((x % 8) + 1));
            eKey.data[x % eKey.f_size] = rotl(eKey.data[x % eKey.f_size], ((x % 8) + 1));
            eKey.data[x % eKey.f_size] ^= ~tData[0];
            eKey.data[x % eKey.f_size] = bit_mixer(eKey.data[x % eKey.f_size]);
        }
        //
        for (x = 0; x < eKey.f_size; x++) {
            eKey.data[x] = 0xff;
        }
        free (eKey.data);
        free (tData);
        // Clutch the bytes chain
        //compressData = nibblesCoupler(compressData);
        compressData = catenate_bytes(compressData);
    }
    // Wipe keydata
    for (x = 0; x < keystream.f_size; x++) {
        keystream.data[x] = 0xff;
    }
    free(keystream.data);
    //
    Struct IV_struct;
    IV_struct.data = NULL;
    IV_struct.f_size = (int) keystream.f_size;
    //
    Struct buff_struct;
    buff_struct.data = NULL;
    //
    buff_struct.f_size = compressData.f_size - IV_struct.f_size;
    //
    buff_struct.data = malloc ((buff_struct.f_size + 1)  *sizeof (char));
    for (x = 0; x < buff_struct.f_size; x++) buff_struct.data[x] = 0;
    for (x = 0; x <= buff_struct.f_size; x++) buff_struct.data[x] = compressData.data[x + IV_struct.f_size];
    //
    for (x = 0; x < compressData.f_size; x++) compressData.data[x] = 0;
    free (compressData.data);

    // New struct for decompressed data
    Struct decompressData;
    decompressData.data = NULL;
    decompressData.f_size = (buff_struct.f_size * D_SIZE_MUL); // Set buffer size with compressed ratio multiplicator
    decompressData.data = malloc ((decompressData.f_size + 1)  *sizeof (char));
    // Call "FL2_decompressMt()" from 'fast-lzma2' lib
    printf("\nBuffer size: %d bytes\n", buff_struct.f_size);
    printf("\nDecompressing...");
    size_t decompressed_size = 
        FL2_decompressMt(decompressData.data, decompressData.f_size, buff_struct.data, buff_struct.f_size, LZMA2_THREADS_COUNT);
    printf("\nDecompressed size bytes: %ld\n",decompressed_size);
    printf("\nDecompressed");
    // Write decoded data to file
    snprintf (filename, 249, "%s.%s", datafile, EXTENSION_DECODED_FILE);
    FILE* const fout = fopen(filename, "wb");
    if (!fout)  {
        fprintf (stderr, "\033[0;31m Error \033[0m %s %s\n", filename, strerror (errno));
        fclose (fout);
        for (x = 0; x < buff_struct.f_size; x++) buff_struct.data[x] = 0;
        free (buff_struct.data);
        for (x = 0; x < decompressed_size; x++) decompressData.data[x] = 0;
        free (decompressData.data);
        exit (0);
    }
    //
    free (buff_struct.data);
    //
    buff_struct.f_size = decompressed_size - IV_struct.f_size;
    //
    buff_struct.data = malloc ((buff_struct.f_size + 1)  *sizeof (char));
    for (x = 0; x < buff_struct.f_size; x++) buff_struct.data[x] = 0;
    for (x = 0; x <= buff_struct.f_size; x++) buff_struct.data[x] = decompressData.data[x + IV_struct.f_size];
    //
    printf ("\nDecoded data size: %d bytes\nWriting file '%s' ->", buff_struct.f_size , filename);
    fwrite(buff_struct.data, sizeof buff_struct.data[0], buff_struct.f_size, fout);
    printf ("\033[0;32m Done!\033[0m\n");
    fclose(fout);
    for (x = 0; x < decompressed_size; x++) decompressData.data[x] = 0;
    free (decompressData.data);
    for (x = 0; x < buff_struct.f_size; x++) buff_struct.data[x] = 0;
    free (buff_struct.data);
    return 0;
}
