/*
f_read.c
Read file(binary mod)
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>

struct filestruct {
    unsigned char *data;
    int f_size;
};

typedef struct filestruct Struct;

Struct readfile (char *filename) {

    Struct File_struct;
    File_struct.data = NULL;
    File_struct.f_size = 0;

    // Get file info
    struct stat stats;
    int x = 0;
    if (stat (filename, &stats) == 0)  {
        for (x = 0; x < stats.st_size; x++) {
            File_struct.f_size++;
        }
        // Verbose section / ifdef TODO
        printf("Read '%s' total bytes: %d\n", filename, File_struct.f_size);
    } else {
        fprintf (stderr, "Error %s %s\n", filename, strerror (errno));
        exit (0);
    }

    File_struct.data = malloc ((File_struct.f_size + 1)  *sizeof (char));
    //  Initializing of data array
    for (x = 0; x <= File_struct.f_size; x++) File_struct.data[x] = 0;
    // Open data file
    FILE* const fp = fopen(filename, "rb");
    //size_t count = fread(data, sizeof data[0], total, fp);
    fread(File_struct.data, sizeof File_struct.data[0], File_struct.f_size, fp);
    fclose(fp);
    
    return (File_struct);
    
}