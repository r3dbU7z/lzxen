/*
main.c
Main/command line parser
*/

#include "argon2.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


#define PROG_VERSION "v1.3.8"

const int LZMA2_COMPRESSION_LEVEL = 9; // Default compression level is "9" - Slow/Small size archive
const int ITERATION_NUM = 1; // Main loop iterator; Default one pass
const int DC_SIZE_MUL = 5; // Default compress ratio 1:5

extern int flzma2_compress();
extern int flzma2_decompress();

int 
main (int argc, char **argv) {

    struct filestore {
        unsigned char *data; // file data
        int f_size; // file size
    };
    typedef struct filestore Struct;

    int c;
    char  * datafile = NULL;
    char  * sFile = NULL;
    char  * clevel = NULL;
    char  * iter_str = NULL;
    char  * ratio_number = NULL;
    int compressLevel = LZMA2_COMPRESSION_LEVEL; // default compression level is '9'
    int enc_iterat_count = ITERATION_NUM; // Number of encode iterations
    int compressRatio = DC_SIZE_MUL; // deCompress Ratio D_SIZE_MUL

    // New struct for keystream data
    Struct keystream;
    keystream.data = NULL;
    keystream.f_size = 0;
    // Import from 'f_read' library
    extern Struct readfile();

    // Add get_PBKDF2_keystream() and get_argon2_keystream()
    #include "keystream_gen.h"

    while ((c = getopt (argc, argv, "f:k:s:l:i:r:edh")) != - 1)  {
        switch (c) {
            case 'f':
            datafile = optarg;
            printf("Selected datafile: %s\n", datafile);
            break;
            case 'k':
            if ( (strcmp (optarg, "pbkdf2")) == 0 ) {
                printf ("\nSelected PKCS5_PBKDF2_HMAC generarion keydata.\n");
                keystream = get_PBKDF2_keystream (keystream);
            }
            if ( (strcmp (optarg, "argon2")) == 0 ) {
                printf ("\nSelected Argon2i generarion keydata.\n");
                keystream = get_argon2_keystream (keystream);
            }
            break;
            case 's':
            sFile = optarg;
            keystream = (readfile(sFile));
            printf("\nSelected keyfile %s.\n", sFile);
            break;
            case 'l':
            clevel = optarg;
            compressLevel = atoi(clevel);
            break;
            case 'i':
            iter_str = optarg;
            enc_iterat_count = atoi(iter_str);
            break;
            case 'r':
            ratio_number = optarg;
            compressRatio = atoi(ratio_number);
            printf("\nSelected compression ratio: %d\n", compressRatio);
            break;
            case 'e':
            if(datafile == NULL) {
                printf ("\033[0;31mFile name is not defined. Abort.\033[0m\n");
                break;
            }
            if(keystream.data == NULL) {
                printf ("\033[0;31mKeystream not found. Abort.\033[0m\n");
                break;
            }
            printf("\nSelected compression level: %d\n", compressLevel);
            printf("Creating lzma archive %s ...\n", datafile);
            flzma2_compress (datafile, keystream, compressLevel, enc_iterat_count);
            break;
            case 'd':
            if(datafile == NULL) {
                printf ("\033[0;31mArchive file not found. Abort.\033[0m\n");
                break;
            }
            if(keystream.data == NULL) {
                printf ("\033[0;31mKeystream not found. Abort.\033[0m\n");
                break;
            }
            printf("Decompressing lzma archive %s ...\n", datafile);
            flzma2_decompress (datafile, keystream, enc_iterat_count, compressRatio);
            break;
            case 'h':
            printf("\nLZMA2 XOR Encoder. Version: "PROG_VERSION" \n");
            printf("\nEncrypt/Decrypt archive:\n");
            printf ("Usage: %s \n    -f <archive_name> \n    -s <keydata_name> [ Mode #1; Read keydata from file ] \n    -k <pbkdf2> /OR/ -k <argon2> [ Mode #2 ]\n      [ Input password and create keydata via PBKDF2 SHA256 /OR/ Argon2i 256 bit ] \n    -l {0..9} [ compression level; optional | default value = 9 ] \n    -i {1..n} [ nunmber of iterations; optional | default value = 1 ] \n    -r {1..n} [ nunmber of compressed ratio; optional | default value = 5 ] \n    -e [ for compress archive ] \n    /OR/ \n    -d [ for decompress archive ] \n", argv[0]);
            exit (0);
            case '?':
            if (optopt == 'c') 
                fprintf (stderr, "Option -%c requires an argument.\n", optopt);
            else if (isprint (optopt)) 
                fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
            return 1;
            default:
            abort ();
        }
    }
    return 0;
}